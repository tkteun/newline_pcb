EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L newline_pcb:BUTTONS_CAP_AB S1
U 1 1 5E543834
P 1950 850
F 0 "S1" H 1950 1050 50  0000 C CNN
F 1 "BUTTONS_CAP_AB" H 1950 950 50  0000 C CNN
F 2 "newline_pcb:BUTTONS_CAP_AB" H 1950 850 50  0001 C CNN
F 3 "~" H 1950 850 50  0001 C CNN
	1    1950 850 
	-1   0    0    -1  
$EndComp
$Comp
L newline_pcb:BUTTONS_CAP_LRUD S2
U 1 1 5E543F12
P 1950 1400
F 0 "S2" H 1950 1600 50  0000 C CNN
F 1 "BUTTONS_CAP_LRUD" H 1950 1500 50  0000 C CNN
F 2 "newline_pcb:BUTTONS_CAP_LRUD" H 1950 1400 50  0001 C CNN
F 3 "~" H 1950 1400 50  0001 C CNN
	1    1950 1400
	-1   0    0    -1  
$EndComp
$Comp
L LED:WS2812B D101
U 1 1 5E55F02D
P 8400 4650
F 0 "D101" H 8744 4696 50  0000 L CNN
F 1 "WS2812B" H 8744 4605 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 8450 4350 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 8500 4275 50  0001 L TNN
	1    8400 4650
	1    0    0    -1  
$EndComp
$Comp
L newline_pcb:RFID-RC522-MODULE P101
U 1 1 5E559F53
P 2000 3150
F 0 "P101" H 2600 3400 60  0000 L CNN
F 1 "RFID-RC522-MODULE" H 1850 3300 60  0000 L CNN
F 2 "newline_pcb:RC522" H -100 3500 60  0000 L CNN
F 3 "" V 2550 2600 60  0000 C CNN
	1    2000 3150
	-1   0    0    -1  
$EndComp
$Comp
L newline_pcb:RDM6300 U102
U 1 1 5E55AEB8
P 2350 5350
F 0 "U102" H 2375 5797 60  0000 C CNN
F 1 "RDM6300" H 2375 5691 60  0000 C CNN
F 2 "newline_pcb:RDM6300_B38.5x18.2mm" H 2350 5000 60  0000 C CNN
F 3 "" H 2350 5350 60  0000 C CNN
	1    2350 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 850  2350 850 
Wire Wire Line
	1950 950  2350 950 
Wire Wire Line
	1950 1050 2350 1050
Text Label 2350 850  2    50   ~ 0
A
Text Label 2350 950  2    50   ~ 0
B
Text Label 2350 1050 2    50   ~ 0
G
Wire Wire Line
	1950 1400 2350 1400
Wire Wire Line
	1950 1500 2350 1500
Wire Wire Line
	1950 1600 2350 1600
Wire Wire Line
	1950 1700 2350 1700
Wire Wire Line
	1950 1800 2350 1800
Text Label 2350 1400 2    50   ~ 0
L
Text Label 2350 1500 2    50   ~ 0
R
Text Label 2350 1600 2    50   ~ 0
U
Text Label 2350 1700 2    50   ~ 0
D
Text Label 2350 1800 2    50   ~ 0
G
Text Label 7000 2950 0    50   ~ 0
G
Wire Wire Line
	5750 2850 5350 2850
Text Label 5350 2850 0    50   ~ 0
G
Wire Wire Line
	5750 2950 5350 2950
Text Label 5350 2950 0    50   ~ 0
G
Wire Wire Line
	5750 2050 5350 2050
Text Label 5350 2050 0    50   ~ 0
G
Wire Wire Line
	5750 1950 5350 1950
Text Label 5350 1950 0    50   ~ 0
G
Wire Wire Line
	5750 3050 5350 3050
Text Label 7000 1950 0    50   ~ 0
3V3
Text Label 5350 3050 0    50   ~ 0
3V3_L
Text Label 7000 3050 0    50   ~ 0
5V
Wire Wire Line
	8400 4950 8800 4950
Text Label 8800 4950 2    50   ~ 0
G
Wire Wire Line
	8400 4350 8800 4350
Text Label 8800 4350 2    50   ~ 0
5V
Wire Wire Line
	2000 3900 2400 3900
Text Label 2400 3900 2    50   ~ 0
G
Wire Wire Line
	2000 4200 2400 4200
Text Label 2400 4200 2    50   ~ 0
3V3
Wire Wire Line
	1600 5450 1200 5450
Text Label 1200 5450 0    50   ~ 0
G
Wire Wire Line
	1950 5950 1550 5950
Text Label 1550 5950 0    50   ~ 0
5V
Wire Wire Line
	3150 5550 3550 5550
Text Label 3550 5550 2    50   ~ 0
G
Wire Wire Line
	3150 5150 3550 5150
Wire Wire Line
	3150 5250 3550 5250
Text Label 3550 5150 2    50   ~ 0
TX
Text Label 3550 5250 2    50   ~ 0
RX
Wire Wire Line
	2000 3600 2400 3600
Wire Wire Line
	2000 3450 2400 3450
Wire Wire Line
	2000 3300 2400 3300
Wire Wire Line
	2000 3150 2400 3150
Text Label 2400 3150 2    50   ~ 0
SPI_SS
Text Label 2400 3300 2    50   ~ 0
SPI_CLK
Text Label 2400 3450 2    50   ~ 0
SPI_MOSI
Text Label 2400 3600 2    50   ~ 0
SPI_MISO
Wire Wire Line
	2000 3750 2400 3750
Wire Wire Line
	2000 4050 2400 4050
Text Label 2400 4050 2    50   ~ 0
SPI_#RST
Text Label 2400 3750 2    50   ~ 0
SPI_IRQ
Text Label 5500 4000 0    50   ~ 0
G
Text Label 5500 3700 0    50   ~ 0
5V
Wire Wire Line
	6400 3850 5500 3850
Wire Wire Line
	5500 3700 6400 3700
Wire Wire Line
	5500 4000 6400 4000
$Comp
L Connector:TestPoint_Flag TP_5V101
U 1 1 5E5B088A
P 6400 3700
F 0 "TP_5V101" H 6660 3794 50  0000 L CNN
F 1 "TestPoint_Flag" H 6660 3703 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 6600 3700 50  0001 C CNN
F 3 "~" H 6600 3700 50  0001 C CNN
	1    6400 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP_WS2812B_DOUT101
U 1 1 5E5B3042
P 6400 3850
F 0 "TP_WS2812B_DOUT101" H 6660 3944 50  0000 L CNN
F 1 "TestPoint_Flag" H 6660 3853 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 6600 3850 50  0001 C CNN
F 3 "~" H 6600 3850 50  0001 C CNN
	1    6400 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP_G101
U 1 1 5E5B341C
P 6400 4000
F 0 "TP_G101" H 6660 4094 50  0000 L CNN
F 1 "TestPoint_Flag" H 6660 4003 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 6600 4000 50  0001 C CNN
F 3 "~" H 6600 4000 50  0001 C CNN
	1    6400 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2150 5350 2150
Wire Wire Line
	5750 2250 5350 2250
Text Label 5350 2150 0    50   ~ 0
TX
Text Label 5350 2250 0    50   ~ 0
RX
Text Label 7000 2750 0    50   ~ 0
SPI_CLK
Text Label 7000 2250 0    50   ~ 0
SPI_IRQ
$Comp
L newline_pcb:TTGO_TDISPLAY U101
U 1 1 5E5681A7
P 5750 1950
F 0 "U101" H 6350 2175 50  0000 C CNN
F 1 "TTGO_TDISPLAY" H 6350 2084 50  0000 C CNN
F 2 "newline_pcb:TTGO_TDISPLAY_CUTOUT" H 5850 1950 50  0001 C CNN
F 3 "" H 5850 1950 50  0001 C CNN
	1    5750 1950
	1    0    0    -1  
$EndComp
Text Label 7350 2350 2    50   ~ 0
SPI_MISO
$Comp
L Device:C_Small C101
U 1 1 5E5E2499
P 7450 3550
F 0 "C101" H 7542 3596 50  0000 L CNN
F 1 "100n" H 7542 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7450 3550 50  0001 C CNN
F 3 "~" H 7450 3550 50  0001 C CNN
	1    7450 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3650 7850 3650
Text Label 7850 3650 2    50   ~ 0
G
Wire Wire Line
	7450 3450 7850 3450
Text Label 7850 3450 2    50   ~ 0
5V
Wire Wire Line
	5750 2350 5350 2350
Text Label 5350 2350 0    50   ~ 0
SPI_#RST
Wire Wire Line
	6950 2750 7350 2750
Wire Wire Line
	6950 2350 7350 2350
Wire Wire Line
	6950 2050 7350 2050
Wire Wire Line
	6950 3050 7350 3050
Wire Wire Line
	6950 2950 7350 2950
Wire Wire Line
	6950 1950 7350 1950
Wire Wire Line
	6950 2150 7350 2150
Wire Wire Line
	6950 2250 7350 2250
$Comp
L Connector_Generic:Conn_01x12 J102
U 1 1 5E66BF02
P 7550 2450
F 0 "J102" H 7630 2442 50  0000 L CNN
F 1 "Conn_01x12" H 7630 2351 50  0000 L CNN
F 2 "newline_pcb:TTGO_TDISPLAY_1ROW" H 7550 2450 50  0001 C CNN
F 3 "~" H 7550 2450 50  0001 C CNN
	1    7550 2450
	1    0    0    -1  
$EndComp
Text Label 7000 2850 0    50   ~ 0
SPI_SS
Wire Wire Line
	6950 2850 7350 2850
Text Label 7000 2650 0    50   ~ 0
SPI_MOSI
Text Label 7000 2450 0    50   ~ 0
A
Text Label 7000 2550 0    50   ~ 0
B
Text Label 5350 2750 0    50   ~ 0
L
Text Label 5350 2550 0    50   ~ 0
R
Text Label 5350 2450 0    50   ~ 0
U
Text Label 5350 2650 0    50   ~ 0
D
$Comp
L Connector_Generic:Conn_01x12 J101
U 1 1 5E66A12B
P 5150 2450
F 0 "J101" H 5068 3167 50  0000 C CNN
F 1 "Conn_01x12" H 5068 3076 50  0000 C CNN
F 2 "newline_pcb:TTGO_TDISPLAY_1ROW" H 5150 2450 50  0001 C CNN
F 3 "~" H 5150 2450 50  0001 C CNN
	1    5150 2450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5750 2450 5350 2450
Wire Wire Line
	5750 2550 5350 2550
Wire Wire Line
	5750 2650 5350 2650
Wire Wire Line
	5750 2750 5350 2750
Wire Wire Line
	7350 2550 6950 2550
Wire Wire Line
	7350 2450 6950 2450
Text Label 3850 4450 0    50   ~ 0
SPI_#RST
Text Label 3850 3550 0    50   ~ 0
SPI_MOSI
Wire Wire Line
	6950 2650 7350 2650
$Comp
L LED:WS2812B D102
U 1 1 5E6917ED
P 8400 3850
F 0 "D102" H 8744 3896 50  0000 L CNN
F 1 "WS2812B" H 8744 3805 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 8450 3550 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 8500 3475 50  0001 L TNN
	1    8400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 4150 8800 4150
Text Label 8800 4150 2    50   ~ 0
G
Wire Wire Line
	8400 3550 8800 3550
Text Label 8800 3550 2    50   ~ 0
5V
Text Label 7550 3850 0    50   ~ 0
WS2812B_DIN
Wire Wire Line
	7550 3850 8100 3850
Text Label 9600 3850 2    50   ~ 0
WS2812B_DOUT
Wire Wire Line
	8700 3850 9600 3850
Text Label 5500 3850 0    50   ~ 0
WS2812B_DIN
Wire Wire Line
	8100 4650 7550 4650
Text Label 7550 4650 0    50   ~ 0
WS2812B_DOUT
$Comp
L Jumper:SolderJumper_2_Bridged JP101
U 1 1 5E6C9FB9
P 4400 3550
F 0 "JP101" H 4400 3755 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 4400 3664 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_RoundedPad1.0x1.5mm" H 4400 3550 50  0001 C CNN
F 3 "~" H 4400 3550 50  0001 C CNN
	1    4400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3550 5100 3550
Text Label 4550 3550 0    50   ~ 0
WS2812B_DIN
$Comp
L Jumper:SolderJumper_2_Open JP102
U 1 1 5E6CD1AE
P 4400 3850
F 0 "JP102" H 4400 4055 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4400 3964 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4400 3850 50  0001 C CNN
F 3 "~" H 4400 3850 50  0001 C CNN
	1    4400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3850 5100 3850
Text Label 4550 3850 0    50   ~ 0
WS2812B_DIN
$Comp
L Jumper:SolderJumper_2_Open JP103
U 1 1 5E6D279F
P 4400 4150
F 0 "JP103" H 4400 4355 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4400 4264 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4400 4150 50  0001 C CNN
F 3 "~" H 4400 4150 50  0001 C CNN
	1    4400 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4150 5100 4150
Text Label 4550 4150 0    50   ~ 0
WS2812B_DIN
$Comp
L Jumper:SolderJumper_2_Open JP104
U 1 1 5E6D27AB
P 4400 4450
F 0 "JP104" H 4400 4655 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 4400 4564 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4400 4450 50  0001 C CNN
F 3 "~" H 4400 4450 50  0001 C CNN
	1    4400 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4450 5100 4450
Text Label 4550 4450 0    50   ~ 0
WS2812B_DIN
Wire Wire Line
	4250 4150 3850 4150
Wire Wire Line
	4250 3850 3850 3850
Text Label 3850 3850 0    50   ~ 0
SPI_SS
Text Label 3850 4150 0    50   ~ 0
SPI_CLK
Wire Wire Line
	4250 4450 3850 4450
Wire Wire Line
	4250 3550 3850 3550
$Comp
L Jumper:SolderJumper_3_Bridged12 JP105
U 1 1 5E6F5611
P 1950 6150
F 0 "JP105" V 1904 6218 50  0000 L CNN
F 1 "SolderJumper_3_Bridged12" V 1995 6218 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm_NumberLabels" H 1950 6150 50  0001 C CNN
F 3 "~" H 1950 6150 50  0001 C CNN
	1    1950 6150
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 6350 1550 6350
Text Label 1200 6150 0    50   ~ 0
RDM6300_SUPPLY
Wire Wire Line
	1200 6150 1800 6150
Text Label 1550 6350 0    50   ~ 0
3V3_L
Text Label 1000 5550 0    50   ~ 0
RDM6300_SUPPLY
Wire Wire Line
	1000 5550 1600 5550
Text Label 3750 5450 2    50   ~ 0
RDM6300_SUPPLY
Wire Wire Line
	3750 5450 3150 5450
$EndSCHEMATC
